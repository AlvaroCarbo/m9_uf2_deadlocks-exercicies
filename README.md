# Exercici 1 Gestió Transferències bancàries


La classe CompteBancari serveix per representar la quantitat de diners que té un usuari d’una entitat bancària. Cada CompteBancari té, com a mínim, un identificador numèric únic (que podeu aconseguir sumant 1 a l’últim identificador creat) i un saldo (en euros i cèntims d’euro). 
La classe Usuari representa als usuaris d’aquests comptes bancaris. Un usuari com a mínim té un compte bancari, però pot tenir més d’un. I un compte bancari pot tenir més d’un titular. L’usuari té un nom i cognoms.
Hi haurà una tercera classe, que farà de control i vista de l’aplicació al mateix temps, per simplificar, per consola (Prova). S’ha de poder donar d’alta usuaris amb els seus comptes i fer les operacions següents: ingressar diners, reintegrar diners i transferir entre 2 comptes (una de les quals sempre ha de ser del titular). Cada operació de l’usuari, per simular entorn real trigarà un temps aleatori entre 1 i 3 segons. Si s’intenta reintegrar més diners que el saldo disponible, saltarà una excepció del tipus SaldoInsuficientException i no s’efectua l’operació.
Tots els usuaris formen part de la mateixa aplicació.

Com a simulació de treballar en un entorn real, amb molts accessos concurrents sobre els mateixos o diferents comptes heu d’anar amb compte sobretot amb:
- si hi hagués accessos concurrents de diferents usuaris sobre el mateix compte bancari, garantir exclusió mútua per evitar interferències i inconsistències del saldo.
- en el moment de fer transferències d’un compte a un altre, garantiu que no es produeixin mai deadlocks que bloquegin l’aplicació. Es recomana ordenar les operacions de transferència per ordre d’identificador de compte (algorisme de Dijsktra).

Feu un joc de proves on poder comprovar:
- 2 accessos concurrents d’usuaris diferents sobre el mateix compte bancari
- 1 reintegrament de quantitat superior al saldo
- ingressar
- reintegrar
- transferència circular concurrent.

